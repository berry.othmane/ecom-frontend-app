import React from 'react';
import { render, screen, waitForElement } from '@testing-library/react';
import AllProduct from '../components/admin/products/ProductTable';
import { ProductContext } from '../components/admin/products/index';
import axios from 'axios';
jest.mock('axios');

// Mock Axios pour simuler les appels API
describe('AllProduct Integration Test', () => {
    test('Affichage des produits', async () => {
      const products = [
        {
          _id: '1',
          pName: 'Product 1',
          pDescription: 'Description for Product 1',
          pImages: ['image1.jpg'],
          pStatus: 'Active',
          pQuantity: 10,
          pCategory: { cName: 'Category 1' },
          pOffer: null,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        // Ajoutez plus de produits si nécessaire
      ];
  
      const dispatch = jest.fn();
  
      // Mock de la réponse de l'appel API pour retourner les produits simulés
      axios.get.mockResolvedValueOnce({ data: { Products: products } });
  
      render(
        <ProductContext.Provider value={{ data: { products: [] }, dispatch }}>
          <AllProduct />
        </ProductContext.Provider>
      );
    });
  });
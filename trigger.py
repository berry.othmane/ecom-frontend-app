import requests

def get_project_id(gitlab_url, personal_access_token):
    path = gitlab_url.split('https://gitlab.com/')[-1].rstrip('.git')
    encoded_path = path.replace('/', '%2F')
    api_url = f"https://gitlab.com/api/v4/projects/{encoded_path}"
    headers = {'Private-Token': personal_access_token}
    response = requests.get(api_url, headers=headers)
    if response.status_code == 200:
        project_id = response.json().get('id')
        print(f"Project ID: {project_id}")
        return project_id
    else:
        print(f"Failed to fetch project ID. Status Code: {response.status_code}")
        return None

def trigger_pipeline(project_id, trigger_token, ref='main'):
    if project_id:
        url = f"https://gitlab.com/api/v4/projects/{project_id}/trigger/pipeline"
        data = {'token': trigger_token, 'ref': ref}
        response = requests.post(url, data=data)
        if response.status_code == 201:
            print("Pipeline triggered successfully!")
            print("Pipeline details:", response.json())
        else:
            print("Failed to trigger pipeline.")
            print("Status Code:", response.status_code)
            print("Response:", response.text)
    else:
        print("No valid project ID provided. Cannot trigger pipeline.")

GITLAB_URL = 'https://gitlab.com/berry.othmane/ecom-app-k8s.git'
PERSONAL_ACCESS_TOKEN = 'glpat-tzb1DW4tm3xtzFBraywk'  # Your actual GitLab personal access token
TRIGGER_TOKEN = 'glptt-9525bacd78fce3e3cae0c115e97b5dd7062c5f18'  # Replace with your pipeline trigger tokenBRANCH_NAME = 'main'
BRANCH_NAME = 'main'
project_id = get_project_id(GITLAB_URL, PERSONAL_ACCESS_TOKEN)
trigger_pipeline(project_id, TRIGGER_TOKEN, BRANCH_NAME)
